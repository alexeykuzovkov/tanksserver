const _gameW = window.innerWidth* window.devicePixelRatio;
const _gameH = window.innerHeight* window.devicePixelRatio;

const _pRatio = window.devicePixelRatio;

const Events = {
    //inbound for client
    MAP    			: 'MAP',
    SANDBAG         : 'SANDBAG',
    REMOVE_SANDBAG  : 'REMOVE_SANDBAG',
    TANK 	  		: 'TANK',
    MY_TANK_ID      : 'MY_TANK_ID',
    REMOVE_TANK		: 'REMOVE_TANK',

    SHOOT 			: 'SHOOT',
    BULLET_DESTROY  : 'BULLET_DESTROY',

    //outbound for client
    TANK_MOVE   	: 'TANK_MOVE',
    TANK_STOP		: 'TANK_STOP',
    BARREL_MOVE  	: 'BARREL_MOVE',
    BARREL_STOP		: 'BARREL_STOP'
};

class Tank {
    constructor(game, x, y, imageName, barrelImageName) {
        this.sprite = game.add.sprite(x,y,imageName);
        this.sprite.anchor.x = 0.5;
        this.sprite.anchor.y = 0.5;
        this.clientid = "";

        this.barrel = game.add.sprite(x,y, barrelImageName);
        this.barrel.anchor.y = 0;
        this.barrel.anchor.x = 0.5;

        this.barrelRotation = 0;
        this.barrelIsMoving = false;
    }

    setRotation(rotation) {
        this.sprite.rotation = rotation;
        this.updateBarrelRotation();
    }
    setBarrelRotation(barrelRotation) {
        this.barrelRotation = barrelRotation;
        this.updateBarrelRotation();
    }

    updateBarrelRotation() {
        this.barrel.rotation = this.sprite.rotation+this.barrelRotation;
    }
}

class Bullet {
    constructor(game, x, y, rotation, imageName) {
        this.sprite = game.add.sprite(x,y,imageName);
        this.sprite.anchor.x = 0.5;
        this.sprite.anchor.y = 0.5;
        this.sprite.rotation = rotation;
        this.id = "";
        this.pixelsPerSecond = 1;
    }

    update(delta) {
        let bulletRotationVector = new Vector2(0,1);
        bulletRotationVector.resetAngle(this.sprite.rotation, true);
        bulletRotationVector.rotate(90, false);

        this.sprite.x+=this.pixelsPerSecond*delta*bulletRotationVector.x;
        this.sprite.y+=this.pixelsPerSecond*delta*bulletRotationVector.y;
    }
}


// Setup game
var game = new Phaser.Game(_gameW, _gameH, Phaser.CANVAS, 'game');

class GameState {
    constructor(game) {
        this.groundSprites      = [];
        this.sandbags           = [];
        this.tanks              = [];
        this.bullets            = [];

        this.isMoving = false;

        this.rotation = new Vector2(0,1);
        this.rotation.resetAngle(0, true);

        this.barrelRotation = new Vector2(0,1);
        this.barrelRotation.resetAngle(0, true);

        this.myTankId = "";
        this.myTank;
    }

    preload() {
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.game.load.image("dirt", "assets/Environment/dirt.png");
        this.game.load.image("grass", "assets/Environment/grass.png");
        this.game.load.image("sand", "assets/Environment/sand.png");
        this.game.load.image("treeLarge", "assets/Environment/treeLarge.png");
        this.game.load.image("treeSmall", "assets/Environment/treeSmall.png");
        this.game.load.image("sandbagBeige", "assets/Obstacles/sandbagBeige.png");

        this.game.load.image("tankRed", "assets/Tanks/tankRed_outline.png");
        this.game.load.image("barrelRed", "assets/Tanks/barrelRed_outline.png");

        this.game.load.image("bullet", "assets/Bullets/bulletRed_outline.png");
    }

    create() {
        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.barrelLeftKey = game.input.keyboard.addKey(Phaser.Keyboard.A);
        this.barrelRightKey = game.input.keyboard.addKey(Phaser.Keyboard.D);
        this.barrelBottomKey = game.input.keyboard.addKey(Phaser.Keyboard.S);

        this.groundGroup = this.game.add.group();
        this.obstaclesGroup = this.game.add.group();
        this.bulletsGroup = this.game.add.group();
        this.tanksGroup = this.game.add.group();


        this.worldGroup = this.game.add.group();
        this.worldGroup.add(this.groundGroup);
        this.worldGroup.add(this.obstaclesGroup);
        this.worldGroup.add(this.bulletsGroup);
        this.worldGroup.add(this.tanksGroup);


        this.socket = io.connect();

        this.socket.on('connect', () => {this.handleConnectEvent()});
        this.socket.on(Events.MAP, (data)=> {this.handleMapEvent(data)});
        this.socket.on(Events.SANDBAG, (data)=>{this.handleSandbagEvent(data)});
        this.socket.on(Events.TANK, (data)=>{this.handleTankEvent(data)});
        this.socket.on(Events.MY_TANK_ID, (data)=>{this.handleMyTankIdEvent(data)});
        this.socket.on(Events.REMOVE_SANDBAG, (data)=>{this.handleRemoveSandbagEvent(data)});
        this.socket.on(Events.REMOVE_TANK, (data)=>{this.handleRemoveTankEvent(data)});

        this.socket.on(Events.SHOOT, (data)=>{this.handleShootEvent(data)});
        this.socket.on(Events.BULLET_DESTROY, (data)=>{this.handleBulletDestroyEvent(data)});

        this.camera = new Phaser.Camera(this.game, 0, 1000, 100, _gameW, _gameH);

    }

    update() {
        this.game.camera.focusOnXY(1000,1000);
        this.moveBullets();
        this.handleKeypress();

        if (this.myTank) {
            this.worldGroup.position.x = -this.myTank.sprite.x+_gameW/2;
            this.worldGroup.position.y = -this.myTank.sprite.y+_gameH/2;
        }
    }

    render() {
        // console.log(game.time.fps);
    }

    //update methods

    moveBullets() {
        let delta = this.game.time.physicsElapsed;
        for (var i = 0; i < this.bullets.length; i++) {
            let bullet = this.bullets[i];
            bullet.update(delta);
        }
    }

    handleKeypress() {

        var oldIsMoving = this.isMoving;
        let oldRotation = this.rotation.angle(true);

        if (this.cursors.up.isDown)
        {
            this.isMoving = true;
        }
        else {
            this.isMoving = false;
        }


        if (this.cursors.left.isDown && this.isMoving)
        {
            this.rotation.rotate(-5,false);
        }
        else if (this.cursors.right.isDown && this.isMoving)
        {
            this.rotation.rotate(5,false);
        }

        if ((this.isMoving && (oldRotation!==this.rotation.angle(true))) || (this.isMoving==true && oldIsMoving==false)) {
            this.socket.emit(Events.TANK_MOVE, {angle: this.rotation.angle(true)});
        }
        else if (oldIsMoving==true && this.isMoving==false) {
            this.socket.emit(Events.TANK_STOP, {});
        }


        let oldBarrelMoving = this.barrelIsMoving;
        this.barrelIsMoving = false;

        if (this.barrelLeftKey.isDown) {
            this.barrelRotation.rotate(-2,false);
            this.barrelIsMoving = true;
        }
        if (this.barrelRightKey.isDown) {
            this.barrelRotation.rotate(2,false);
            this.barrelIsMoving = true;
        }
        if (this.barrelBottomKey.isDown) {
            this.barrelIsMoving = true;
        }

        if(this.barrelIsMoving) {
            this.socket.emit(Events.BARREL_MOVE, {angle:this.barrelRotation.angle(true)+this.rotation.angle(true)});
        }

        if (!this.barrelIsMoving && oldBarrelMoving==true) {
            this.socket.emit(Events.BARREL_STOP, {});
        }

    }

    //Socket Events

    handleConnectEvent() {
        this.groundSprites = this.clearSpritesArray(this.groundSprites);
        this.sandbags = this.clearSpritesArray(this.sandbags);
        this.tanks = this.clearSpritesArray(this.tanks);
    }

    handleMapEvent(data) {
        for(var i = data.length-1; i>=0; i--) {
            for (var j = 0; j < data[i].length; j++) {
                let sprite = this.game.add.sprite(128*j, 128*i, data[i][j]);
                this.groundSprites.push(sprite);
                this.groundGroup.add(sprite);
            }
        }
    }

    handleSandbagEvent(data) {
        let sprite = this.game.add.sprite(data.x, data.y, "sandbagBeige");
        sprite.rotation = data.rotation;
        sprite.anchor.x = 0.5;
        sprite.anchor.y = 0.5;
        this.obstaclesGroup.add(sprite);
        this.sandbags.push(sprite);
    }

    handleRemoveSandbagEvent(data) {
        this.sandbags[parseInt(data.index)].destroy();
        this.sandbags.splice(parseInt(data.index), 1);
    }

    handleTankEvent(data) {
        var tankExists = false;
        for (var i = 0; i<this.tanks.length; i++) {
            var tank = this.tanks[i];
            if (tank.clientid==data.clientid) {
                //Нашли танк в массиве
                //Меняем его параметры на новые
                tankExists = true;

                tank.sprite.x = data.x;
                tank.sprite.y = data.y;
                tank.setRotation(data.rotation);

                tank.barrel.x = data.x;
                tank.barrel.y = data.y;
                tank.setBarrelRotation(data.barrelRotation);
            }
        }

        if (!tankExists) {
            let tank = new Tank(this.game, data.x, data.y, 'tankRed', 'barrelRed');
            tank.clientid = data.clientid;
            this.tanksGroup.add(tank.sprite);
            this.tanksGroup.add(tank.barrel);

            this.tanks.push(tank);
        }
    }

    handleMyTankIdEvent(data) {
        this.myTankId = data.id;

        for (var i = 0; i < this.tanks.length; i++) {
            if (this.tanks[i].clientid==this.myTankId) {
                this.myTank = this.tanks[i];
            }
        }
    }

    handleRemoveTankEvent(data) {
        for (var i = 0; i<this.tanks.length; i++) {
            var tank = this.tanks[i];
            if (tank.clientid==data.clientid) {
                tank.sprite.destroy();
                tank.barrel.destroy();
                this.tanks.splice(i, 1);
                break;
            }
        }
    }

    handleShootEvent(data) {
        let bullet = new Bullet(this.game, data.x, data.y, data.rotation, "bullet");
        bullet.id = data.id;
        bullet.pixelsPerSecond = data.pixelsPerSecond;
        this.bullets.push(bullet);
        this.bulletsGroup.add(bullet.sprite);
    }

    handleBulletDestroyEvent(data) {
        for (var i = 0; i<this.bullets.length; i++) {
            var bullet = this.bullets[i];
            if (bullet.id==data.id) {
                bullet.sprite.destroy();
                this.bullets.splice(i, 1);
                break;
            }
        }
    }


    //Sprite tools

    clearSpritesArray(array) {
        for (var i = 0; i<array.length; i++) {
            array[i].destroy();
        }
        return [];
    }
}

game.state.add('game', GameState, true);