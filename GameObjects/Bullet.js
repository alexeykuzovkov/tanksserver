const p2 = require('p2');
const Vector2 = require('../Vector2');

class Bullet {
	constructor(x,y,rotation, pixelsPerSecond) {
		this.x = x
		this.y = y
		this.rotation = rotation
		this.pixelsPerSecond = pixelsPerSecond
		this.lifetime = 120
		this.id = 0
		this.tankid = ""

		this.damage = 20


        this.physicsBody = new p2.Body({
            mass: 1,
            position: [x, y],
            angle: rotation
        });
        var shape = new p2.Box({ width:12, height:26 });
        this.physicsBody.addShape(shape);

        this.physicsBody.bullet = this
	}

    getSocketInfo() {
        return {
            x: this.x,
            y: this.y,
            rotation: this.rotation,
            pixelsPerSecond: this.pixelsPerSecond,
			id: this.id

        }
    }

    update(delta) {
        this.lifetime-=1;

        let bulletRotationVector = new Vector2(0,1);
        bulletRotationVector.resetAngle(this.rotation, true);
        bulletRotationVector.rotate(90, false);

        // let speedModifier = bullet.pixelsPerSecond/60;
        //
        this.x+=this.pixelsPerSecond*delta*bulletRotationVector.x;
        this.y+=this.pixelsPerSecond*delta*bulletRotationVector.y;

        this.physicsBody.position[0] = this.x
        this.physicsBody.position[1] = this.y

        // this.physicsBody.velocity[0] = this.pixelsPerSecond*delta*bulletRotationVector.x
        // this.physicsBody.velocity[1] = this.pixelsPerSecond*delta*bulletRotationVector.x
        //
        // this.x = this.physicsBody.position[0];
        // this.y = this.physicsBody.position[1];
	}
}

module.exports.Bullet = Bullet;