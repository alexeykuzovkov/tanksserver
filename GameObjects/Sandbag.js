const p2 = require('p2');

class Sandbag {
    constructor(x,y,rotation) {
        this.x = x;
        this.y = y;
        this.width = 66;
        this.height = 44;
        this.rotation = rotation;
        this.id = 0;

        this.physicsBody = new p2.Body({
            mass: 0,
            position: [x, y],
            angle: rotation
        });

        var shape = new p2.Box({ width:66, height:44 });
        this.physicsBody.addShape(shape);
    }

    getSocketInfo() {
        return {
            x: this.x,
            y: this.y,
            rotation: this.rotation
        }
    }
}

module.exports.Sandbag = Sandbag;