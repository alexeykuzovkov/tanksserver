const p2 = require('p2');
const Vector2 = require('../Vector2');

var TankColors = {
	Beige : 'Beige',
	Red   : 'Red',
	Green : 'Green',
	Blue  : 'Blue'
};

function getSmoothRotationVector(currentAngleRad, targetAngleRad, speed) {
    //Вычисляем, в какую сторону крутиться
    let multiplier = getRotationDirection(currentAngleRad, targetAngleRad);

    //В результате знак переменной multiplier определяет направление вращения
    //Тернар определяет, что разница между текущим углом и целевым в радианах > 0.2 чтобы не было дергучки
    let rotatorAngle = Math.abs(targetAngleRad-currentAngleRad)>(speed*2)?multiplier*speed:0;

    //Получаем вектор текущего вращения танка из угла
    let myTankBarrelRotationVector = new Vector2(0,1);
    myTankBarrelRotationVector.resetAngle(currentAngleRad, true);
    //Вращаем вектор
    myTankBarrelRotationVector.rotate(rotatorAngle, true);
    //Назначем танку новый угол вращения
    return myTankBarrelRotationVector
}

function getRotationDirection(tankRotation, targetRotation) {
    let tankAngleDeg = tankRotation*(180 / Math.PI);
    let targetAngleDeg = targetRotation*(180 / Math.PI);

    if(tankAngleDeg < targetAngleDeg) {
        if(Math.abs(tankAngleDeg - targetAngleDeg)<=180)
            multiplier = 1;
        else multiplier = -1;
    }
    else {
        if(Math.abs(tankAngleDeg - targetAngleDeg)<=180)
            multiplier = -1;
        else multiplier = 1;
    }

    return multiplier
}

class Tank {
	constructor(x,y,w,h) {
		this.x = x
		this.y = y

		this.width = w
		this.height = h
		this.color = TankColors.Red
		this.clientid = 0
		this.rotation = 0
		this.barrelRotation = 0

		this.barrelMoving = false

		//Время до возможности следующего выстрела
		this.shootingCooldown = 0
		//Время, которое должно выставиться для кулдауна после выстрела
		this.shootingCooldownResetTime = 60

		//Флаг, определяющий, двигаться танку в update или нет
		this.isMoving = false
		//Куда танку крутиться при движении
		this.targetRotation = 0

		this.health = 100

		this.physicsBody = new p2.Body({
            mass: 50,
            position: [x, y]
        });

        var shape = new p2.Box({ width: w, height:h });
        this.physicsBody.addShape(shape);
	}

	update(delta) {
        this.x = this.physicsBody.position[0];
        this.y = this.physicsBody.position[1];

        this.move(delta)

        this.physicsBody.angle = this.rotation
    }

    move(delta) {
        //Движение танка
        if (this.isMoving) {
            let targetVector = new Vector2(0,0);
            targetVector.resetAngle(this.targetRotation, true);
            targetVector.rotate(90, false);


            let tankRotationVector = getSmoothRotationVector(this.rotation, this.rotation, 0);
            if (Math.abs(targetVector.x)>0.5) {
                tankRotationVector = getSmoothRotationVector(this.rotation, this.rotation - targetVector.x, 0.05);
            }


            this.rotation = tankRotationVector.angle(true)

            //Вращаем вектор на 90 градусов, чтобы танк ехал в нужную сторону (потому что спрайт по умолчанию смотрит не туда)
            tankRotationVector.rotate(90, false)

            //Двигаем танк
            let yMult = Math.abs(targetVector.y)>0.5?targetVector.y:0;
            this.physicsBody.velocity[0] = tankRotationVector.x*10000*delta*yMult;
            this.physicsBody.velocity[1] = tankRotationVector.y*10000*delta*yMult;
        }
        else {
            this.physicsBody.velocity = [0,0]
        }
    }

    getSocketInfo() {
        return {
            x: this.x,
            y: this.y,
            clientid: this.clientid,
            color: this.color,
            rotation: this.rotation,
            barrelRotation: this.barrelRotation
        }
    }
}

module.exports.TankColors = TankColors;
module.exports.Tank = Tank;