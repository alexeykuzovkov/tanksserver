const express = require('express');
var path = require('path');
const app = express();
const http = require('http').Server(app);

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

app.use(express.static(path.join(__dirname, '/client')));

app.get('/p2.js', function(req, res){
	res.sendFile(__dirname + '/node_modules/p2/build/p2.min.js');
});


const io = require('socket.io')(http);
const Vector2 = require('./Vector2');
const gameloop = require('node-gameloop');

const p2 = require('p2');

const Tank = require('./GameObjects/Tank').Tank;
const Sandbag = require('./GameObjects/Sandbag').Sandbag;
const TankColors = require('./GameObjects/Tank').TankColors;

const Bullet = require('./GameObjects/Bullet').Bullet;

let tanks = [];

var bulletID = 0;
let bullets = [];



var allSockets = [];

let map = [ ["dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt"],
			["dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt"],
			["dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt"],
			["dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt"],
			["dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt"],
			["dirt","grass","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt"],
			["grass","grass","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt","dirt"]]


let sandbagsMap = [
    [0,1,1,1,1,1,1,1],
    [0,1,0,0,0,0,0,1],
    [1,1,0,0,0,0,0,1],
    [0,0,0,0,1,0,0,1],
    [1,1,0,0,1,0,0,1],
    [0,1,1,1,1,0,0,1],
    [0,0,0,0,1,0,0,1],
    [1,0,0,0,1,0,0,1],
    [1,1,1,1,1,0,0,0],
]
let sandbags = [];

for (var y = 0; y<sandbagsMap.length; y++) {
    for (var x = 0; x < sandbagsMap[y].length; x++) {
        if (sandbagsMap[y][x]===1)
            sandbags.push(new Sandbag(x*66+400, -y*44+400, 0))
    }
}


const Events = {
	//system
	CONNECT   		: 'connection', 
	DISCONNECT		: 'disconnect',

	//inbound for client
	MAP    			: 'MAP',
    SANDBAG         : 'SANDBAG',
    REMOVE_SANDBAG  : 'REMOVE_SANDBAG',
	TANK 	  		: 'TANK',
	MY_TANK_ID      : 'MY_TANK_ID',
	REMOVE_TANK		: 'REMOVE_TANK',
	
	SHOOT 			: 'SHOOT',
	BULLET_DESTROY  : 'BULLET_DESTROY',

	//outbound for client
	TANK_MOVE   	: 'TANK_MOVE',
	TANK_STOP		: 'TANK_STOP',
	BARREL_MOVE  	: 'BARREL_MOVE',
	BARREL_STOP		: 'BARREL_STOP'
};


//Init physics

var world = new p2.World({
    gravity:[0, 0]
});

for (var i = 0; i < sandbags.length; i++) {
    let sandbag = sandbags[i];
    world.addBody(sandbag.physicsBody)
}

// Create an infinite ground plane.
var groundBody = new p2.Body({
    mass: 0 // Setting mass to 0 makes the body static
});
var groundShape = new p2.Plane();
groundBody.addShape(groundShape);
world.addBody(groundBody);

// world.on("beginContact", (bodyA, bodyB, shapeA, shapeB, equation)=> {
//     if(bodyA!==undefined && bodyB!==undefined) {
//         if (bodyA.bullet!==undefined) {
//             console.log("A is bullet");
//         }
//         if (bodyB.bullet!==undefined) {
//             console.log("B is bullet");
//         }
//     }
// });

//init socket

// let puppetTank = new Tank(400, 400, 83, 78);
// puppetTank.clientid = "nothing";
// tanks[tanks.length] = puppetTank;
// world.addBody(puppetTank.physicsBody);
// allSockets[allSockets.length] = "dummy socket";


io.sockets.on(Events.CONNECT, socket => {

	let myTank = new Tank(Math.random()*1200, Math.random()*800, 83, 78);
	myTank.clientid = socket.id;

	tanks[tanks.length] = myTank;
	allSockets[allSockets.length] = socket;

    world.addBody(myTank.physicsBody);


	for (var i = tanks.length - 1; i >= 0; i--) {
		socket.emit(Events.TANK, tanks[i].getSocketInfo())
	}

	socket.emit(Events.MY_TANK_ID, {id: myTank.clientid});

    for (var i = 0; i < sandbags.length; i++) {
        let sandbag = sandbags[i];
        socket.emit(Events.SANDBAG, sandbag.getSocketInfo());
    }

	socket.broadcast.emit(Events.TANK, myTank.getSocketInfo());

	socket.emit(Events.MAP, map);


	socket.on(Events.TANK_MOVE, data => {
		myTank.isMoving = true;
		myTank.targetRotation = data.angle;
	});

	socket.on(Events.TANK_STOP, data => {
		myTank.isMoving = false;
	});

	socket.on(Events.BARREL_MOVE, data => {
		let targetBarrelAngleRad = data.angle-myTank.rotation;

		myTank.barrelRotation = targetBarrelAngleRad;//getSmoothRotationVector(myTank.barrelRotation, targetBarrelAngleRad, 0.2).angle(true)

		myTank.barrelMoving = true;

		// socket.broadcast.emit(Events.TANK, myTank)
		// socket.emit(Events.TANK, myTank)
	});

	socket.on(Events.BARREL_STOP, data => {
		myTank.barrelMoving = false;
	});

	socket.on(Events.DISCONNECT, () => {
     	let i = tanks.indexOf(myTank);

     	socket.broadcast.emit(Events.REMOVE_TANK, tanks[i].getSocketInfo());
		socket.emit(Events.REMOVE_TANK, tanks[i].getSocketInfo());
        world.removeBody(tanks[i].physicsBody);

     	tanks.splice(i, 1);
     	allSockets.splice(i,1);
   });
});

const id = gameloop.setGameLoop(function(delta) {
	for (var i = tanks.length - 1; i >= 0; i--) {
		let tank = tanks[i];

		tank.update(delta);
        //Отправлять данные каждый кадр
        // TODO:Подумать, возможно, стоит посылать все танки одним запросом, но это неточно
        io.sockets.emit(Events.TANK, tank.getSocketInfo());

		//Стрельба
		if (tank.barrelMoving) {
			if ( tank.shootingCooldown<=0) {
				var bullet = new Bullet(tank.x, tank.y, tank.rotation+tank.barrelRotation, 600);
				bullet.id = bulletID;
				bullet.tankid = tank.clientid;
				bullets[bullets.length] = bullet;
				bulletID+=1;

				world.disableBodyCollision(bullet.physicsBody, tank.physicsBody);

				io.sockets.emit(Events.SHOOT, bullet.getSocketInfo());

				world.addBody(bullet.physicsBody)

				tank.shootingCooldown = tank.shootingCooldownResetTime
			}
		}
		
		else if (tank.shootingCooldown<5) {
			//Чтобы выстреливал не сразу, как схватились за джойстик
			tank.shootingCooldown = 5
		}

		if (tank.shootingCooldown>0) {
			tank.shootingCooldown-=1;
		}
	}

	//Move and destroy bullets
	for (var i = 0; i < bullets.length; i++) {
		let bullet = bullets[i];

		//Cтолкновение пуль с танками
		for (var j = 0; j < tanks.length; j++) {
		    let tank = tanks[j];
            if (bullet.tankid!==tank.clientid) {
                if (bulletCollisionCheck(bullet, tank)) {

                    tank.health-=bullet.damage;

                    if (tank.health<=0) {
                        let i = tanks.indexOf(tank);
                        io.sockets.emit(Events.REMOVE_TANK, tanks[i].getSocketInfo());
                        world.removeBody(tanks[i].physicsBody);
                        tanks.splice(i, 1);
                        allSockets.splice(i,1);
                    }

                    io.sockets.emit(Events.BULLET_DESTROY, {'id': bullet.id});

                    world.removeBody(bullet.physicsBody);
                    bullets.splice(j,1);

                }
            }
        }
        //Cтолкновение пуль с мешами
        for (var j = 0; j < sandbags.length; j++) {
            let bag = sandbags[j];
            if (bulletCollisionCheck(bullet, bag)) {
                io.sockets.emit(Events.BULLET_DESTROY, {'id': bullet.id});
                world.removeBody(bullet.physicsBody);
                bullets.splice(i,1);


                io.sockets.emit(Events.REMOVE_SANDBAG, {'index': j});
                world.removeBody(bag.physicsBody);
                sandbags.splice(j, 1);

            }
        }


		bullet.update(delta);
        if (bullet.lifetime<=0) {
            io.sockets.emit(Events.BULLET_DESTROY, {'id': bullet.id});
            world.removeBody(bullet.physicsBody);
            bullets.splice(i,1);
        }
	}


    world.step(delta);

}, 1000 / 60);


function bulletCollisionCheck(bullet, tank) {
    return (bullet.x>=(tank.x-tank.width/2) &&
    bullet.x<=(tank.x+tank.width/2) &&
    bullet.y>=(tank.y-tank.height/2) &&
    bullet.y<=(tank.y+tank.height/2));
}

http.listen(3000, function(){
    console.log('listening on *:3000');
});